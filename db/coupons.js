//this is just a dummy data. Data in this segment should originally come from database,
const coupons = [
    {
        code: "PRIME123",
        minimumAmount: 10001,
        discountAmount: 123
    }
]
module.exports = coupons;
