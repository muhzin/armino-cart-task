
//this is just a dummy data. Data in this segment should originally come from database,
const products = [
    {
        id: '1',
        name: "Note Book",
        price: 100,
        minQuantity: 3,
        maxQuantity: null,
        inStock: 10000, //assumption
    },
    {
        id: '2',
        name: "Bag",
        price: 1500,
        minQuantity: null,
        maxQuantity: 2,
        inStock: 10000, //assumption
    },
    {
        id: '3',
        name: "Sanitizer",
        price: 250,
        minQuantity: 10,
        maxQuantity: null,
        inStock: 10000, //assumption
    }
]

module.exports = products

