//this is just a dummy data. Data in this segment should originally come from database,
const products = [
    {
        id: '1', //note book
        discountPercentage: 10,
        flatDiscount: null,
        cutOffAmount: 500,
        maxDiscountableAmount: 60
    },
    {
        id: '3', //sanitizer
        discountPercentage: null,
        flatDiscount: 100,
        cutOffAmount: 3001,
        maxDiscountableAmount: null
    },
]

module.exports = products

