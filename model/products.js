export function ProductModel(id, name, minQuantity, maxQuantity, price, inStock) {
    this.id = id;
    this.name = name;
    this.minQuantity = minQuantity;
    this.maxQuantity = maxQuantity;
    this.price = price;
    this.inStock = inStock;
}