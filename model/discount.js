var coupons = require('../db/coupons'); //originally it should come from DB

function ProductDiscount(percentage, maxDiscount, flatDiscount, originalCost) {
    this.percentage = percentage;
    this.maxDiscount = maxDiscount;
    this.originalCost = originalCost;
    this.discountAmount = function () {
        let amount
        if (flatDiscount) amount = flatDiscount;
        else amount = originalCost * (percentage / 100);

        if (maxDiscount ? amount > maxDiscount : false) return maxDiscount
        else return amount

    };
    this.finalAmount = function () {
        return this.originalCost - this.discountAmount()
    };
}

function TotalDiscount(totalCost, couponCode) {
    this.totalCost = totalCost;
    this.coupon = coupons.filter(function (coupon) { return (coupon.code == couponCode); }); //originally a db query //originally it should be queried from DB
    this.finalAmount = function () {
        return totalCost - this.coupon.discountAmount;
    }
    this.isApplicable = function () {
        if (totalCost > this.coupon.minimumAmount) return true
        else return false
    }
}

module.exports = ProductDiscount, TotalDiscount