const products = require('../db/products')
const coupons = require('../db/coupons')
const productDiscounts = require('../db/productDiscounts')

const ProductDiscount = require('../model/discount')




const productDiscountCalculator = (productId, quantity) => {

    let [product] = products.filter(function (product) { return (product.id == productId); }); //originally a db query
    let [productDiscount] = productDiscounts.filter(function (discount) { return (discount.id == productId); }); //originally a db query

    let discountPercentage = productDiscount ? productDiscount.discountPercentage : null;
    let maxDiscount = productDiscount ? productDiscount.maxDiscountableAmount : null;
    let flatDiscount = productDiscount ? productDiscount.flatDiscount : null;
    let originalCost = product ? product.price * quantity : null;

    if (productDiscount ? productDiscount.cutOffAmount <= originalCost : false) { //if discount is applicable
        let productDiscountObject = new ProductDiscount(discountPercentage, maxDiscount, flatDiscount, originalCost)

        console.log("RES", product.id, productDiscountObject.finalAmount())
        return (productDiscountObject)
    }
    else { //do discount applicable
        let productDiscountObject = new ProductDiscount(0, 0, 0, originalCost)
        return (productDiscountObject);
    }


}

module.exports = productDiscountCalculator
