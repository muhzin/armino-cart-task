var express = require('express');
var router = express.Router();

var productDiscountCalculator = require('../utils/discountCalculator')
var productQuantityValidator = require('../utils/productValidator');

var coupons = require('../db/coupons');

/* GET home page. */
router.post('/', function (req, res, next) {
    // body: {
    //     cartItems: [
    //         {
    //             productId: String,
    //             productName: String,
    //             quantity: Number
    //         }
    //     ]
    //     couponCode: String
    // }

    // const dummyBody = {
    //     cartItems: [ //originally request.body
    //         { productId: '1', productName: 'Note Book', quantity: 50 },
    //         { productId: '2', productName: 'Bag', quantity: 2 },
    //         { productId: '3', productName: 'Sanitizer', quantity: 15 }
    //     ],
    //     couponCode: "PRIME123"
    // }
    let data = req.body

    let amountToPay = 0;
    let originalCost = 0;
    let couponApplied = false;
    let isQuantityValid = {} //keys are product ID, value are validation result

    data.cartItems.forEach(product => {
        isQuantityValid[product.productId] = productQuantityValidator(product.productId, product.quantity); //checks if product quantities are valid (in stock logic is not included)
        let discountDetails = productDiscountCalculator(product.productId, product.quantity); //creates the discount object dynamically
        amountToPay += discountDetails ? discountDetails.finalAmount() : 0;
        originalCost += discountDetails ? discountDetails.originalCost : 0;
    });
    let [couponDetails] = coupons.filter(function (coupon) { return (coupon.code == data.couponCode); }); //originally a db query

    if (couponDetails ? amountToPay >= couponDetails.minimumAmount : false) { //check if coupon is valid 

        amountToPay -= couponDetails.discountAmount;
        couponApplied = true
    }


    if (Object.values(isQuantityValid).includes(false)) {
        res.send({
            status_code: 400,
            status_message: "Product Quantity Invalid",
            data: {
                isQuantityValid: isQuantityValid,
            }
        })
    }
    else {
        res.send({
            status_code: 200,
            status_message: "Billing Details",
            data: {
                originalCost: originalCost,
                amountToPay: amountToPay,
                discountAmount: originalCost - amountToPay,
                couponApplied: couponApplied,
            }
        })
    }

});

module.exports = router;
