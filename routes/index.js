var express = require('express');
var router = express.Router();

var productDiscountCalculator = require('../utils/discountCalculator')

/* GET home page. */
router.post('/', function (req, res, next) {
    console.log('HIT')
    // body: [
    //   {
    //     productId: String,
    //     productName: String,
    //     quantity: Number
    //   }
    // ]
    const dummyBody = [ //originally request.body
        { productId: 1, productName: 'Note Book', quantity: 10 },
        { productId: 2, productName: 'Bag', quantity: 1 },
        { productId: 3, productName: 'Sanitizer', quantity: 15 }
    ]
    let data = dummyBody

    data.forEach(product => {
        productDiscountCalculator(product.productId, product.quantity)
    })

    res.send({
        msg: 'HIT'
    })
});

module.exports = router;
