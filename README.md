# Armino Cart Task



## Starting the project
Run following commands to start project
1. `npm install`
2. `npm start`

## How to test the app

Project will listen to port `3000`.

**URL:** `localhost:3000/cart`
**Method:** POST

A sample request payload is given below

```
{
    "cartItems": [ 
        { "productId": "1", "productName": "Note Book", "quantity": 50 },
        { "productId": "2", "productName": "Bag", "quantity": 1},
        { "productId": "3", "productName": "Sanitizer", "quantity": 15 }
    ],
    "couponCode": "PRIME123"
}
```
